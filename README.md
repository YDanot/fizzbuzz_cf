## Fizzbuzz without flow control. 

##### No If,Else,Switch,Ternary

###### Fizzbuzz rules are :

* return Fizz if a number is multiple of 3
* return Buzz if a number is multiple of 5

###### For O/C principle add this rule
* return Bazz if a number is multiple of 7