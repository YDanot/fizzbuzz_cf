package fizzbuzz;

import org.assertj.core.util.Strings;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static fizzbuzz.RuleBuilder._return;

class Fizzbuzz {

    private final List<Rule> ruleList;
    private final Function<Integer, String> otherwise;

    Fizzbuzz() {
        this.ruleList = Arrays.asList(
                _return("FIZZ").when_number_is_multiple_of(3),
                _return("BUZZ").when_number_is_multiple_of(5),
                _return("BAZZ").when_number_is_multiple_of(7));
        otherwise = String::valueOf;
    }

    String applyOn(int number) {
        return applicableRules(number).concatWords().orElse(otherwise.apply(number));
    }

    private ApplicableRules applicableRules(int number) {
        return new ApplicableRules(number);
    }

    private class ApplicableRules {

        private final Stream<Rule> applicables;

        private ApplicableRules(Integer number) {
            this.applicables = ruleList.stream().filter((r) -> r.doesApplyOn(number));
        }

        private Optional<String> concatWords() {
            return applicables.map(Rule::word).reduce((s, r) -> Strings.concat(s, r));
        }
    }
}
