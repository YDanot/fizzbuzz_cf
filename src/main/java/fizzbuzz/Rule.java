package fizzbuzz;

import java.util.function.IntPredicate;

class Rule {

    private final IntPredicate predicate;
    private final String word;

    Rule(IntPredicate predicate, String word) {
        this.predicate = predicate;
        this.word = word;
    }

    boolean doesApplyOn(Integer i) {
        return predicate.test(i);
    }

    String word() {
        return word;
    }
}
