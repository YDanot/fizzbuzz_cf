package fizzbuzz;

final class RuleBuilder {
    private String word;
    private int multiple;

    private RuleBuilder() {
    }

    static RuleBuilder _return(String word) {
        return new RuleBuilder().withWord(word);
    }

    Rule when_number_is_multiple_of(int multiple) {
        this.multiple = multiple;
        return this.build();
    }

    private RuleBuilder withWord(String word) {
        this.word = word;
        return this;
    }

    private Rule build() {
        return new Rule(((i) -> i % multiple == 0), word);
    }

}
