package fizzbuzz;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FizzBuzzTest {

    @Test
    public void should_return_FIZZ_when_3() {
        assertThat(fizzbuzz(3)).isEqualTo("FIZZ");
    }

    @Test
    public void should_return_FIZZ_when_6() {
        assertThat(fizzbuzz(6)).isEqualTo("FIZZ");
    }

    @Test
    public void should_return_BUZZ_when_5() {
        assertThat(fizzbuzz(5)).isEqualTo("BUZZ");
    }

    @Test
    public void should_return_BUZZ_when_10() {
        assertThat(fizzbuzz(10)).isEqualTo("BUZZ");
    }

    @Test
    public void should_return_FIZZBUZZ_when_15() {
        assertThat(fizzbuzz(3 * 5)).isEqualTo("FIZZBUZZ");
    }

    @Test
    public void should_return_4_when_4() {
        assertThat(fizzbuzz(4)).isEqualTo("4");
    }

    @Test
    public void should_return_BAZZ_when_7() {
        assertThat(fizzbuzz(7)).isEqualTo("BAZZ");
    }

    @Test
    public void should_return_BAZZ_when_14() {
        assertThat(fizzbuzz(14)).isEqualTo("BAZZ");
    }

    @Test
    public void should_return_FIZZBAZZ_when_21() {
        assertThat(fizzbuzz(3 * 7)).isEqualTo("FIZZBAZZ");
    }

    @Test
    public void should_return_BUZZBAZZ_when_35() {
        assertThat(fizzbuzz(5 * 7)).isEqualTo("BUZZBAZZ");
    }

    @Test
    public void should_return_FIZZBUZZBAZZ_when_105() {
        assertThat(fizzbuzz(3 * 5 * 7)).isEqualTo("FIZZBUZZBAZZ");
    }

    private String fizzbuzz(int i) {
        return new Fizzbuzz().applyOn(i);
    }

}
